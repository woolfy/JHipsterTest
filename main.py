import socket
import sys

import docker


def get_containers(client):
    return client.containers.list()


def find_open_ports(start):
    for port in range(start, 8070, 2):
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
            res = sock.connect_ex(('localhost', port))
            if res == 0:
                yield port


def get_free_port_tuple():
    return next(find_open_ports(2000)), next(find_open_ports(2001))


def check_if_container_is_running(client, container_name):
    containers = list(filter(lambda x: x.name == container_name, get_containers(client)))

    if len(containers) > 0:
        return containers[0]
    else:
        return None


def create_new_container(branch_name):
    print("Creating new container for branch " + branch_name + ", choosing ports")
    rest_port, db_port = get_free_port_tuple()
    print("Selected ports: " + str(rest_port) + ", " + str(db_port))


def update_instance_in_running_container(container):
    print("Updating instance in running container: " + container.name)


def main(current_branch_name):
    client = docker.from_env()
    running_container = check_if_container_is_running(client=client, container_name=current_branch_name)

    if running_container is None:
        create_new_container(current_branch_name)
    else:
        update_instance_in_running_container(running_container)

if __name__ == "__main__":
    print("works")
    main(sys.argv[1])
